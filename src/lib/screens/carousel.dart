import 'dart:math';

import 'package:flutter/material.dart';

final images = [
  'images/sweden.jpg',
  'images/sweden-2.jpeg',
  'images/sweden-3.jpg',
];

final colors = [
  Colors.blue,
  Colors.redAccent,
  Colors.greenAccent,
];

final cardAspectRatio = 12 / 16;
final widthAspectRatio = cardAspectRatio * 1.2;

class CarouselScreen extends StatefulWidget {
  CarouselScreen({Key key}) : super(key: key);

  _CarouselScreenState createState() => _CarouselScreenState();
}

class _CarouselScreenState extends State<CarouselScreen> {
  var currentPage = images.length - 1.0;
  var pageController = new PageController(
    initialPage: images.length - 1,
  );

  @override
  Widget build(BuildContext context) {
    pageController.addListener(() {
      setState(() {
        currentPage = pageController.page;
        // print('currentpage $currentPage');
      });
    });
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(color: Colors.black),
          CardCarousel(currentPage),
          Positioned.fill(
            child: PageView.builder(
              controller: pageController,
              reverse: true,
              itemCount: images.length,
              itemBuilder: (context, index) {
                return Opacity(
                  opacity: 0.1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: colors[index],
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class CardCarousel extends StatelessWidget {
  var currentPage;
  var padding = 20.0;
  var verticalInset = 20.0;

  CardCarousel(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: widthAspectRatio,
      child: LayoutBuilder(
        builder: (context, constraints) {
          var width = constraints.maxWidth;
          var height = constraints.maxHeight;

          var safeWidth = width - 2 * padding;
          var safeHeight = height - 2 * padding;

          var heightOfPrimaryCard = safeHeight;
          var widthOfPrimaryCard = heightOfPrimaryCard * cardAspectRatio;

          var primaryCardLeft = safeWidth - widthOfPrimaryCard;
          var horizontalInset = primaryCardLeft / 2;

          List<Widget> cardList = [];

          for (var i = 0; i < images.length; i++) {
            var delta = i - currentPage;
            var isOnRight = delta > 0;

            var start = padding + max(primaryCardLeft - horizontalInset * -delta * (isOnRight ? 15 : 1), 0.0);
            if (i == 0) {
              print('0 - start: $start');
            }

            var cardItem = Positioned.directional(
              top: padding + verticalInset * max(-delta, 0.0),
              bottom: padding + verticalInset * max(-delta, 0.0),
              textDirection: TextDirection.rtl,
              start: start,
              child: Container(
                color: colors[i],
                child: AspectRatio(
                  aspectRatio: cardAspectRatio,
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[Image.asset(images[i], fit: BoxFit.cover)],
                  ),
                ),
              ),
            );

            cardList.add(cardItem);
          }

          return Stack(
            children: cardList,
          );
        },
      ),
    );
  }
}
