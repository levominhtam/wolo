import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:core';
import 'package:random_color/random_color.dart';
import 'package:wolo/widgets/calendar.dart';

final random = new RandomColor();

final images = [
  'images/sweden.jpg',
  'images/sweden-2.jpeg',
  'images/sweden-3.jpg',
  'images/sweden.jpg',
  'images/sweden-2.jpeg',
  'images/sweden-3.jpg',
  'images/sweden.jpg',
  'images/sweden-2.jpeg',
  'images/sweden-3.jpg',
];

final colors = [
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
  random.randomColor(),
];

final cardAspectRatio = 12 / 16;
final widthAspectRatio = cardAspectRatio * 1.1;

class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key key}) : super(key: key);

  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  var currentPage = images.length - 1.0;
  var pageController = new PageController(
    initialPage: images.length - 1,
  );

  @override
  Widget build(BuildContext context) {
    pageController.addListener(() {
      setState(() {
        currentPage = pageController.page;
        // print('currentpage $currentPage');
      });
    });
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            // color: Color(0xFF2B2F52),
            // color: Color(0xFF0B0C13),
            color: Color(0xFF070707),
            child: Center(
              child: Calendar(),
            ),
          ),
          // Container(
          //     color: Colors.black,
          //     child: Center(
          //       child: CardCarousel(currentPage),
          //     )),
          // Positioned.fill(
          //   child: PageView.builder(
          //     controller: pageController,
          //     reverse: true,
          //     itemCount: images.length,
          //     itemBuilder: (context, index) {
          //       return Container();
          //     },
          //   ),
          // )
        ],
      ),
    );
  }
}

class CardCarousel extends StatelessWidget {
  var currentPage;
  var padding = 10.0;
  var verticalInset = 20.0;

  CardCarousel(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: widthAspectRatio,
      child: LayoutBuilder(
        builder: (context, constraints) {
          var width = constraints.maxWidth;
          var height = constraints.maxHeight;

          var safeWidth = width - 2 * padding;
          var safeHeight = height - 2 * padding;

          var heightOfPrimaryCard = safeHeight;
          var widthOfPrimaryCard = heightOfPrimaryCard * cardAspectRatio;

          var primaryCardLeft = safeWidth - widthOfPrimaryCard;
          var horizontalInset = primaryCardLeft / 2;

          List<Widget> cardList = [];

          for (var i = 0; i < images.length; i++) {
            var delta = i - currentPage;
            var isOnRight = delta > 0;
            var absoluteDelta = delta.abs();

            var start = padding + max(primaryCardLeft - horizontalInset * -delta * (isOnRight ? 30 : 1), 0.0);

            var cardItem = Positioned.directional(
              top: padding + verticalInset * max(-delta, 0.0),
              bottom: padding + verticalInset * max(-delta, 0.0),
              textDirection: TextDirection.rtl,
              start: start,
              child: Visibility(
                visible: absoluteDelta < 3,
                child: Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(3.0, 6.0),
                      blurRadius: 10.0,
                    )
                  ]),
                  child: AspectRatio(
                    aspectRatio: cardAspectRatio,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Container(color: colors[i]),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );

            cardList.add(cardItem);
          }

          return Stack(
            children: cardList,
          );
        },
      ),
    );
  }
}
