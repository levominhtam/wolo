import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wolo/widgets/date_page.dart';
import 'package:wolo/widgets/workout_list.dart';
import 'package:wolo/screens/workout_detail.dart';
import 'package:wolo/util/data.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime date = DateTime.now();
  Map<String, DateMark> dateMarkers = new Map();

  @override
  void initState() {
    super.initState();

    var tomorrow = DateTime.now().add(Duration(days: 1));
    var yesterday = DateTime.now().add(Duration(days: -1));

    var keyTomorrow = DateMark.dateFormatter.format(tomorrow);
    var keyYesterday = DateMark.dateFormatter.format(yesterday);
    dateMarkers.putIfAbsent(keyTomorrow, () {
      return DateMark(keyTomorrow, bordered: true);
    });
    dateMarkers.putIfAbsent(keyYesterday, () {
      return DateMark(keyYesterday, bordered: true);
    });

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xFFFFFFFF), Color.fromRGBO(255, 255, 255, 0.92)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          tileMode: TileMode.clamp,
        ),
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          floatingActionButton: FloatingActionButton.extended(
            icon: Text(
              '🔥',
              style: TextStyle(fontSize: 28),
            ),
            label: Text('Start workout'),
            backgroundColor: Colors.red[400],
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WorkoutDetail(workouts[0])
                ),
              );
            },
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          body: Container(
            color: Colors.transparent,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 24.0),
                  child: DatePaging(
                    markDates: dateMarkers,
                    onPageChanged: (int activePageIndex, List<DateTime> dates) {
                      setState(() {
                        date = dates[0];
                      });
                      print('active page $activePageIndex, first date: ${dates[0]}');
                    },
                  ),
                ),
                WorkoutList(workouts),
              ],
            ),
          )),
    );
  }
}
