import 'package:flutter/material.dart';
import 'package:wolo/modal/modal.dart';

final titleStyle = TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.w600);

class WorkoutDetail extends StatefulWidget {
  final Widget child;
  final Workout workout;

  WorkoutDetail(this.workout, {Key key, this.child}) : super(key: key);

  _WorkoutDetailState createState() => _WorkoutDetailState();
}

class _WorkoutDetailState extends State<WorkoutDetail> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back, color: Colors.black),
        ),
        title: Text(widget.workout.title, style: titleStyle),
      ),
      body: Column(
        children: <Widget>[
          Image.asset('images/logo_splash.png'),
          Container(color: Colors.white, child: Row(
            
          ),)
        ],
      ),
    );
  }
}