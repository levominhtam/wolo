class Workout {
  final String title;
  final List<WorkoutSet> sets;
  final List<String> tags;
  Workout(this.title, this.sets, {this.tags = const []});
}

class WorkoutSet {
  final int reps;
  final double weight;
  WorkoutSet(this.weight, {this.reps = 1});
}