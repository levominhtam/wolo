import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wolo/util/date.dart';
import 'package:preload_page_view/preload_page_view.dart';
import 'circular_border.dart';

final DEBUG = false;
final formatterDateTitle = new DateFormat.yMMMM();
final formatterDateItem = new DateFormat.d();
final formatterDayName = new DateFormat('EEEE');
final formatterDateKey = new DateFormat.yMd();
final dayNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
final emptyMap = new Map();

class DateMark {
  static DateFormat dateFormatter = formatterDateKey;

  final String date;
  final bool blur;
  final bool bordered;
  final bool filled;
  final double fillPercent;

  DateMark(this.date,
      {this.blur = false,
      this.bordered = false,
      this.filled = false,
      this.fillPercent = 0.0});
}

class DatePaging extends StatefulWidget {
  final int pastNumberWeeks;
  final int futureNumberWeeks;
  final void Function(int activePageIndex, List<DateTime> dates) onPageChanged;
  final Map<String, DateMark> markDates;

  DatePaging(
      {this.pastNumberWeeks = 12,
      this.futureNumberWeeks = 12,
      this.onPageChanged,
      this.markDates});

  @override
  DatePagingState createState() => new DatePagingState();
}

class DatePagingState extends State<DatePaging> {
  final DateFormat monthFormat = formatterDateTitle;
  final DateFormat dateFormat = formatterDateItem;

  Week centerWeek;
  int index;
  List<Week> weeks = [];
  DateTime selectedDate;
  PreloadPageController pageController;

  @override
  void initState() {
    super.initState();
    DEBUG ?? print('initState');
    Week current = Week.current();
    List<Week> past = [];
    List<Week> future = [];

    Week point = current;
    for (var i = 0; i < widget.pastNumberWeeks; i++) {
      point = point.previousWeek();
      past.insert(0, point);
    }
    point = current;
    for (var i = 0; i < widget.futureNumberWeeks; i++) {
      point = point.nextWeek();
      future.add(point);
    }

    List<Week> weeks = []
      ..addAll(past)
      ..add(current)
      ..addAll(future);
    this.weeks = weeks;
    this.centerWeek = current;
    this.index = past.length;
    this.selectedDate = DateTime.now();
    this.pageController = PreloadPageController(initialPage: this.index);
    // print(weeks);
  }

  nextWeek() {
    scrollToIndex(index + 1);
  }

  previousWeek() {
    scrollToIndex(index - 1);
  }

  scrollToday() {
    scrollToIndex(weeks.indexOf(centerWeek));
  }

  scrollToIndex(int index) {
    pageController.animateToPage(index,
        curve: Curves.easeInOut, duration: Duration(milliseconds: 500));
  }

  Widget _buildHeaderPart(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 8.0),
            child: Center(
              child: Text(monthFormat.format(weeks[index].from),
                  style: TextStyle(
                      fontSize: 22,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600,
                      color: Colors.black87)),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDayName(BuildContext context) {
    var children = dayNames.asMap().map((index, value) {
      bool isSunday = index == dayNames.length - 1;
      return MapEntry(index, _dayTile(value, isSunday: isSunday));
    }).values.toList();

    return Padding(
      padding: EdgeInsets.only(top: 12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: children,
      ),
    );
  }

  Widget _dayTile(String text, {bool isSunday = false}) {
    DEBUG ?? print('dayTile: $text');
    return Expanded(
      key: UniqueKey(),
      child: Center(
        child: Text(
          text.toUpperCase(),
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 12,
            fontWeight: FontWeight.w700,
            color: isSunday ? Colors.blue : Colors.black87,
          ),
        ),
      ),
    );
  }

  Widget _page(BuildContext context, Week week) {
    DEBUG ?? print('_page weeknumber ${week.weekNumber}');
    return Row(
        key: PageStorageKey(week.weekNumber),
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: week.dates.map((date) => _buildDate(date)).toList());
  }

  Widget _buildPages(BuildContext context) {
    final week = weeks[index];
    bool selectedDateHasPast = week.from.isAfter(selectedDate);
    bool selectedDateHasFuture = week.to.isBefore(selectedDate);
    return Row(
      children: <Widget>[
        Expanded(
          child: Stack(children: <Widget>[
            SizedBox(
              height: 60,
              child: PreloadPageView.builder(
                physics: BouncingScrollPhysics(),
                controller: pageController,
                onPageChanged: (int pageIndex) {
                  DEBUG ?? print('onPageChange $pageIndex');
                  setState(() {
                    index = pageIndex;
                  });

                  if (this.widget.onPageChanged != null) {
                    this
                        .widget
                        .onPageChanged(pageIndex, weeks[pageIndex].dates);
                  }
                },
                pageSnapping: true,
                itemCount: weeks.length,
                itemBuilder: (BuildContext context, int index) {
                  DEBUG ?? print('build page $index');
                  return _page(context, weeks[index]);
                },
              ),
            ),
            selectedDateHasPast
                ? dotIndicator(Alignment.topLeft, key: UniqueKey())
                : Container(),
            selectedDateHasFuture
                ? dotIndicator(Alignment.topRight, key: UniqueKey())
                : Container(),
          ]),
        ),
      ],
    );
  }

  Widget dotIndicator(Alignment alignment, {Key key}) {
    return Padding(
      key: key,
      padding: const EdgeInsets.all(4.0),
      child: Align(
        alignment: alignment,
        child: Container(
          width: 8,
          height: 8,
          decoration: BoxDecoration(
            color: Colors.red[400],
            shape: BoxShape.circle,
          ),
        ),
      ),
    );
  }

  Widget _buildDate(DateTime date) {
    DEBUG ?? print('date: ${formatterDateItem.format(date)}');
    DateTime today = DateTime.now();
    bool selected = false;
    bool isToday = isSameDay(today, date);
    if (selectedDate != null) {
      selected = isSameDay(date, selectedDate);
    }

    bool bordered = false;
    bool blur = false;
    bool filled = false;
    double fillPercent = 0.0;
    var key = DateMark.dateFormatter.format(date);
    if (this.widget.markDates != null) {
      var hasMarker = this.widget.markDates.containsKey(key);
      if (hasMarker) {
        DateMark marker = this.widget.markDates[key];
        bordered = marker.bordered;
        blur = marker.blur;
        filled = marker.filled;
        fillPercent = marker.fillPercent;
      }
    }

    Color textColor = Colors.black;
    Color borderColor = Colors.transparent;
    Color fillColor;
    if (selected) {
      textColor = Colors.white;
      fillColor = Colors.red[400];
    } else if (isToday) {
      textColor = Colors.red[400];
    }

    if (bordered) {
      borderColor = Colors.black26;
    } else if (filled) {
      fillColor = Colors.red[400];
      textColor = Colors.white;
    }

    return Expanded(
      child: AspectRatio(
        aspectRatio: 1,
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
          child: InkWell(
            customBorder: CircleBorder(),
            onTap: () {
              setState(() {
                selectedDate = date;
              });
            },
            child: LayoutBuilder(
              builder: (BuildContext ctx, BoxConstraints constraints) {
                return CircularBorder(
                  color: borderColor,
                  fillColor: fillColor,
                  fillPercent: fillPercent,
                  size: constraints.minWidth,
                  width: 2.0,
                  child: Center(
                    child: Text(dateFormat.format(date),
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: textColor,
                        )),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    DEBUG ?? print('build');
    return Column(
      children: <Widget>[
        this._buildHeaderPart(context),
        this._buildDayName(context),
        this._buildPages(context),
      ],
    );
  }
}
