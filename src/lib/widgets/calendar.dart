import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:wolo/util/date.dart';

final kDates = [
  new DateTime(2019, 05, 06),
  new DateTime(2019, 05, 07),
  new DateTime(2019, 05, 08),
  new DateTime(2019, 05, 09),
  new DateTime(2019, 05, 10),
  new DateTime(2019, 05, 11),
];
final DateFormat monthFormator = new DateFormat('MMMM');
final DateFormat yearFormator = new DateFormat('yyyy');

class Calendar extends StatelessWidget {
  DateTime startDate;
  DateTime endDate;

  Calendar({Key key, this.startDate, this.endDate}) : super(key: key) {
    if (startDate == null) {
      startDate = DateUtils.getFirstDayOfCurrentMonth();
    }
    startDate = DateUtils.toMidnight(startDate);

    if (endDate == null) {
      endDate = DateUtils.getLastDayOfCurrentMonth();
    }
    endDate = DateUtils.toMidnight(endDate);
  }

  Widget buildTitle() {
    var month = monthFormator.format(startDate);
    var year = yearFormator.format(startDate);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(width: 10),
              Text(month,
                  style: TextStyle(
                      fontFamily: 'Montserrat', 
                      fontSize: 24, color: Colors.white, 
                      fontWeight: FontWeight.w600)),
              SizedBox(width: 12),
              Text(year,
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 24,
                    color: Colors.white,
                  )),
            ],
          );
  }

  @override
  Widget build(BuildContext context) {
    var dates = DateUtils.getDates(startDate, endDate, maxNumberOfDates: 7);
    List<Widget> children = dates
        .asMap()
        .map((index, value) {
          return MapEntry(index, CalendarDay(date: value));
        })
        .values
        .toList();
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 60),
          buildTitle(),
          SizedBox(height: 12),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: children,
          )
        ],
      ),
    );
  }
}

class CalendarDay extends StatelessWidget {
  final DateTime date;
  final DateFormat formattor = new DateFormat.d();

  CalendarDay({Key key, this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var selected = false;

    return Expanded(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: AspectRatio(
            aspectRatio: 1.0,
            child: Container(
              decoration: BoxDecoration(
                color: selected ? Color(0xFFFAFBFC) : Colors.transparent,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Center(
                child: Text(
                  formattor.format(date),
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Montserrat',
                    color: Color(0xFFFAFBFC),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
