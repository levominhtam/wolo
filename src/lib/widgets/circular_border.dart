import 'dart:math';
import 'package:flutter/material.dart';

class CircularBorder extends StatelessWidget {
  final Color color;
  final double size;
  final double width;
  final Widget child;
  final Color fillColor;
  final double fillPercent;

  const CircularBorder(
      {Key key,
      this.fillColor,
      this.fillPercent = 0.0,
      this.color = Colors.blue,
      this.size = 70,
      this.width = 7.0,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          CustomPaint(
            size: Size(size, size),
            foregroundPainter: new MyPainter(completeColor: color, fillColor: fillColor, width: width),
          ),
          child == null ? Container() : child,
        ],
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  Color lineColor = Colors.transparent;
  Color completeColor;
  Color fillColor;
  double width;
  MyPainter({this.completeColor, this.width, this.fillColor});
  @override
  void paint(Canvas canvas, Size size) {
    Paint complete = new Paint()
      ..color = completeColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;

    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    var percent = (size.width * 0.002) / 2;

    double arcAngle = 2 * pi * percent;

    for (var i = 0; i < 12; i++) {
      var init = (-pi / 2) * (i / 3);
      // canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), init, arcAngle, false, complete);
    }
    canvas.drawCircle(center, radius, complete);

    // fill
    if (fillColor != null) {
      Paint filler = new Paint()
      ..color = fillColor
      ..style = PaintingStyle.fill;
      canvas.drawCircle(center, radius - 1, filler);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
