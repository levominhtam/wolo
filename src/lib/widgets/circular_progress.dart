import 'package:flutter/material.dart';
import 'dart:math';

class CircularProgress extends StatelessWidget {
  final Widget child;
  final double radius;
  final double lineWidth;
  final double percent;
  final Color inactiveColor;
  final Color activeColor;

  CircularProgress(
      {Key key,
      this.child,
      this.radius = 70,
      this.lineWidth = 10,
      this.percent = 0,
      this.inactiveColor = Colors.black26,
      this.activeColor = Colors.green})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius,
      height: radius,
      child: Stack(
        alignment: Alignment.center,
        children: [
          CustomPaint(
            size: Size(radius, radius),
            painter: MyPainter(radius, lineWidth, percent, inactiveColor, activeColor),
          ),
          child == null ? Container() : child,
        ],
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  final double radius;
  final double lineWidth;
  final double percent;
  final Color inactiveColor;
  final Color activeColor;

  MyPainter(this.radius, this.lineWidth, this.percent, this.inactiveColor, this.activeColor);

  @override
  void paint(Canvas canvas, Size size) {
    print('mypainter size $size');
    Paint paint = new Paint()
    ..color = inactiveColor
    ..style = PaintingStyle.stroke
    ..strokeCap = StrokeCap.round
    ..strokeWidth = lineWidth;

    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    // draw inactive circle
    canvas.drawCircle(center, radius, paint);
    // draw progress circle
    Rect rect = Rect.fromCircle(center: center, radius: radius);
    double startAngle = -pi * 1/2;
    double sweepAngle = 2 * pi * percent;
    Paint progressPaint = new Paint()
    ..color = activeColor
    ..style = PaintingStyle.stroke
    ..strokeCap = StrokeCap.round
    ..strokeWidth = lineWidth;
    canvas.drawArc(rect, startAngle, sweepAngle, false, progressPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
