import 'package:flutter/material.dart';
import 'package:wolo/modal/modal.dart';

final worklistItemNumberStyle = TextStyle(fontFamily: 'Montsarrat', fontWeight: FontWeight.w600);
final worklistItemTextStyle = TextStyle(fontFamily: 'Montsarrat', fontSize: 9);

class WorkoutList extends StatelessWidget {
  final List<Workout> workouts;

  WorkoutList(this.workouts);

  Widget workoutSetRow(WorkoutSet workoutSet, int index) {
    return Row(
      children: <Widget>[
        Flexible(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 2.0),
            child: Row(
              children: <Widget>[
                Container(child: Text('$index', style: worklistItemNumberStyle, textAlign: TextAlign.left,), width: 10),
                Text(' set → '.toUpperCase(), style: worklistItemTextStyle),
                Text('${workoutSet.weight.toInt()}', style: worklistItemNumberStyle),
                Text(' lbl x '.toUpperCase(), style: worklistItemTextStyle),
                Text('${workoutSet.reps}', style: worklistItemNumberStyle),
                Text(' reps '.toUpperCase(), style: worklistItemTextStyle),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _workoutItem(BuildContext context, Workout workout, int index) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8.0), boxShadow: [
        BoxShadow(
          color: Color(0x0D000000),
          blurRadius: 16,
        )
      ]),
      child: InkWell(
        splashColor: Colors.blue,
        highlightColor: Colors.deepOrange,
        onTap: () {
          print('on InkWell pressed.');
        },
        child: Container(
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 50,
                child: Stack(children: <Widget>[
                  Center(
                    child: Text(
                      index.toString(),
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 28,
                        color: Colors.red[400],
                      ),
                    ),
                  ),
                ]),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              workout.title,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 5.0),
                        Column(
                            children: workout.sets
                                .asMap()
                                .map((index, element) => MapEntry(index, workoutSetRow(element, index)))
                                .values
                                .toList()),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: workouts.length,
        itemBuilder: (BuildContext context, int index) {
          bool lastItem = workouts.length == (index + 1);
          var item = _workoutItem(context, workouts[index], index);
          if (lastItem) {
            return Container(child: item, padding: const EdgeInsets.only(bottom: 72));
          }
          return item;
        },
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          '${workouts.length} excercise',
          style: TextStyle(
            color: Colors.black87,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
        child: Column(
          children: <Widget>[
            _buildTitle(context),
            _buildList(context),
          ],
        ),
      ),
    );
  }
}
