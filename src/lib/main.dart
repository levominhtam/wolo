import 'package:flutter/material.dart';
import 'package:wolo/screens/home.dart';
import 'package:wolo/screens/splash.dart';
import 'package:wolo/screens/carousel.dart';
import 'package:wolo/screens/dashboard.dart';

void main() {
  runApp(MyApp());
}

final routes = {
  '/': (BuildContext context) => SplashScreen(),
  '/home': (BuildContext context) => MyHomePage(),
  '/carousel': (BuildContext context) => CarouselScreen(),
  '/dashboard': (BuildContext context) => DashboardScreen(),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/dashboard',
      routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}