import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

const FirstIndexOfWeek = DateTime.monday;
final DateFormat weekFormaterToString = DateFormat.yMd();

class DateUtils {
  static DateTime toMidnight(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month, dateTime.day);
  }

  static bool isWeekend(DateTime date) {
    return date.weekday == DateTime.saturday || date.weekday == DateTime.sunday;
  }

  static bool isToday(DateTime date) {
    var now = DateTime.now();
    return date.day == now.day && date.month == now.month && date.year == now.year;
  }

  static bool isPastDay(DateTime date) {
    var today = toMidnight(DateTime.now());
    return date.isBefore(today);
  }

  static DateTime addDaysToDate(DateTime date, int days) {
    DateTime newDate = date.add(Duration(days: days));

    if (date.hour != newDate.hour) {
      var hoursDifference = date.hour - newDate.hour;

      if (hoursDifference <= 3 && hoursDifference >= -3) {
        newDate = newDate.add(Duration(hours: hoursDifference));
      } else if (hoursDifference <= -21) {
        newDate = newDate.add(Duration(hours: 24 + hoursDifference));
      } else if (hoursDifference >= 21) {
        newDate = newDate.add(Duration(hours: hoursDifference - 24));
      }

    }
    return newDate;
  }

  static bool isSpecialPastDay(DateTime date) {
    return isPastDay(date) || (isToday(date) && DateTime.now().hour >= 12);
  }

  static DateTime getFirstDayOfCurrentMonth() {
    var dateTime = DateTime.now();
    dateTime = getFirstDayOfMonth(dateTime.month);
    return dateTime;
  }


  static DateTime getFirstDayOfMonth(int month) {
    var dateTime = DateTime.now();
    dateTime = DateTime(dateTime.year, month, 1);
    return dateTime;
  }

  static DateTime getFirstDayOfNextMonth() {
    var dateTime = getFirstDayOfCurrentMonth();
    dateTime = addDaysToDate(dateTime, 31);
    dateTime = DateTime(dateTime.year, dateTime.month, 1);
    return dateTime;
  }

  static DateTime getLastDayOfCurrentMonth() {
    return getFirstDayOfNextMonth().subtract(Duration(days: 1));
  }

  static DateTime getLastDayOfNextMonth() {
    var nextNextMonth = addDaysToDate(getFirstDayOfCurrentMonth(), 32 * 2);
    return DateTime(nextNextMonth.year, nextNextMonth.month, 1)
        .subtract(Duration(days: 1));
  }

  static bool isSameDay(DateTime date1, DateTime date2) {
    return date1.day == date2.day && date1.month == date2.month && date1.year == date2.year;
  }

  static bool isCurrentMonth(DateTime date) {
    var now = DateTime.now();
    return date.month == now.month && date.year == now.year;
  }

  static List<DateTime> getDates(DateTime start, DateTime end, {int maxNumberOfDates = 365}) {
    List<DateTime> dates = [];
    
    var date = start;
    var count = 100;
    while(end.difference(date).inDays >= 1) {
      print('date: $date');
      dates.add(date);
      date = date.add(Duration(days: 1));
      if (dates.length >= maxNumberOfDates) {
        return dates;
      }
      count--;
      if (count < 0) return [];
    }
    
    return dates;
  }
}

class Week {
  int firstDayOfWeekIndex = FirstIndexOfWeek;
  int weekNumber = 0;
  DateTime from;
  DateTime to;
  List<DateTime> dates = [];

  Week(DateTime date, {BuildContext context}) {
    int firstDayOfWeekIndex = FirstIndexOfWeek;
    if (context != null) {
      // NOTE: not understand it completely, temporary disable it!
      // firstDayOfWeekIndex = MaterialLocalizations.of(context).firstDayOfWeekIndex;
    }

    // find out start date of week
    DateTime start = new DateTime(date.year, date.month, date.day);
    int pastDays = 0;
    while (start.weekday != firstDayOfWeekIndex && pastDays < 10) {
      pastDays += 1;
      start = date.subtract(new Duration(days: pastDays));
      // print('\t shift in ${pastDays} days: ${weekFormaterToString.format(start)}(weekday:${start.weekday}) with firstDayIndex:${firstDayOfWeekIndex}');
    }

    // get all days from start date of week
    List<DateTime> dates = [];
    for (var i = 0; i < 7; i++) {
      dates.add(start.add(new Duration(days: i)));
    }

    // find out the week of year
    final startOfYear = new DateTime(date.year, 1, 1);
    final firstMonday = startOfYear.weekday;
    final daysInFirstWeek = 8 - firstMonday;
    final diff = date.difference(startOfYear);
    var weeks = ((diff.inDays - daysInFirstWeek) / 7).ceil();
    if (daysInFirstWeek > 3) {
      weeks += 1;
    }

    this.weekNumber = weeks;
    this.firstDayOfWeekIndex = firstDayOfWeekIndex;
    this.from = dates[0];
    this.to = dates[6];
    this.dates = dates;
  }

  nextWeek() {
    DateTime date = to.add(new Duration(days: 1));
    return new Week(date);
  }

  previousWeek() {
    DateTime date = from.subtract(new Duration(days: 1));
    return new Week(date);
  }

  @override
  String toString() =>
      'Week(${weekFormaterToString.format(this.from)} - ${weekFormaterToString.format(this.to)})';

  static current({BuildContext context}) {
    return new Week(DateTime.now(), context: context);
  }
}

final dateUtilCompareFormater = DateFormat.yMd();
isSameDay(DateTime d1, DateTime d2) {
  if (d1 == d2) {
    return true;
  }
  if (d1 == null || d2 == null) {
    return false;
  }
  return dateUtilCompareFormater.format(d1) ==
      dateUtilCompareFormater.format(d2);
}
