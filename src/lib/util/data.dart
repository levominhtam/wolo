import 'package:wolo/modal/modal.dart';

final List<Workout> workouts = [
  Workout('Squat', [
    WorkoutSet(30, reps: 12),
    WorkoutSet(40, reps: 10),
    WorkoutSet(50, reps: 8),
  ]),
  Workout('Dumbbell bench press', [
    WorkoutSet(60, reps: 10),
    WorkoutSet(40, reps: 15),
  ]),
  Workout('Squat', [
    WorkoutSet(15, reps: 12),
    WorkoutSet(30, reps: 8),
    WorkoutSet(45, reps: 6),
  ]),
  Workout('Pulls up', [
    WorkoutSet(20, reps: 15),
    WorkoutSet(25, reps: 10),
    WorkoutSet(40, reps: 5),
  ]),
];